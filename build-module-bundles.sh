#! /bin/bash

set -e -x

MODULES_PATH=modules

# Build the modules.
build_modules ()
{
    rm -rf "$MODULES_PATH" \
        && make INSTALL_MOD_PATH="$MODULES_PATH" modules_install
}

# Minimal modules for initrd. These are extracted to a tmpfs at boot, loaded
# as needed, then the tmpfs is discarded.
build_module_bundle ()
{
    echo "Generating the full module bundle for regular boot..." \
        && tar --transform='s|^'"$MODULES_PATH"'/||' \
               --owner=:0 \
               --group=:0 \
               -cvzf "$1" \
               --exclude=build \
               --exclude=source \
               "$MODULES_PATH"/lib \
        && echo "Done."
}

# Full modules for regular boot.
build_initrd_module_bundle ()
{
    echo "Generating the minimal module bundle for the initrd..." \
        && tar --transform='s|^'"$MODULES_PATH"'/||' \
               --owner=:0 \
               --group=:0 \
               -cvzf "$1" \
               "$MODULES_PATH"/lib/modules/*/modules.* \
               "$MODULES_PATH"/lib/modules/*/kernel/fs/nls/nls_base.ko \
               "$MODULES_PATH"/lib/modules/*/kernel/drivers/{net/usb,usb/**,mmc/**,scsi}/*.ko \
        && echo "Done."
}

build_modules \
    && build_module_bundle modules.tar.gz \
    && build_initrd_module_bundle modules-initrd.tar.gz